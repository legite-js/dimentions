/*!******************************************************************
fichier: ruches-0.2.2-comportement.js
version : 0.2.2
auteur : Pascal TOLEDO
date :1 decembre 2012
source: http://www.legral.fr/intersites/lib/perso/js/gestionScenario/
idee original:http://www.javascriptfr.com//code.aspx?ID=54636
description: comportement d'un element dans la ruche
compatibilite:
dependance:aucune
*******************************************************************/

/***********************************************
****		gestion des cas normaux           ****
***********************************************/
elt_cpt_normal=
	{
	  vecteurX:   function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurX=v;}
	 ,vecteurY:   function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurY=v;}
	 ,vecteurRelX:function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurX+=v;}
	 ,vecteurRelY:function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurY+=v;}

	 ,vecteurAleatY:function(elt,min,max)
	 	{
	 	if((!elt)&&(!min)&&(!max)){return;}
	 	elt.vecteurY=Math.AleatoireSignedMinMax(min,max);
	 	}
	,vecteurAleatX:function(elt,min,max)
		{
	 	if((!elt)&&(!min)&&(!max)){return;}
		elt.vecteurX=Math.AleatoireSignedMinMax(min,max);
		}
	,walk:function(elt,coef)
		{
		var v=(coef)?coef:elt.vitesse;
		elt.coordX+=elt.vecteurX*v;elt.coordY+=elt.vecteurY*v;
		}
	,moveXY:function(elt,x,y)
		{
		if(!elt){return;}
		if(x){elt.coordX=x;}
		if(y){elt.coordY=y;}
		}
	,moveRelXY:function(elt,x,y)
		{
		if(!elt){return;}
		if(x){elt.coordX+=x;}
		if(y){elt.coordY+=y;}
		}
	,moveCosXY:function(elt,x,y)
		{
		if(!elt){return;}
		var cos=Math.cos(elt.age.cycle);
		if(x){elt.coordX=elt.coordX+Math.round(cos*x);}
		if(y){elt.coordY=elt.coordY+Math.round(cos*y);}
		}
	,vaVersElt:function(elt,elt2)
		{
		if((!elt)||(typeof(elt2)==='undefined')){return;}
		var eltCX =elt.larg/2; var eltCY =elt.haut/2;
		var elt2CX=elt2.larg/2;var elt2CY=elt2.haut/2;
		
		elt.vecteurX= Math.round(  (elt2.coordX+elt2CX)-(elt.coordX+eltCX)  )   ;
		elt.vecteurY= Math.round(  (elt2.coordY+elt2CY)-(elt.coordY+eltCY)  )   ;
		if(!elt.vecteurX){elt.vecteurX=0;if(!elt.vecteurY){elt.vecteurY=0}return};
		if(!elt.vecteurY){elt.vecteurY=0;if(!elt.vecteurX){elt.vecteurX=0}return};
		//ratio
		if     (elt.vecteurX<elt.vecteurY){elt.vecteurY=elt.vecteurY/elt.vecteurX;elt.vecteurX=elt.vecteurX/elt.vecteurX;}
		else if(elt.vecteurX>elt.vecteurY){elt.vecteurX=elt.vecteurX/elt.vecteurY;elt.vecteurY=elt.vecteurY/elt.vecteurY;}
		else {elt.vecteurX=elt.vecteurX/elt.vecteurX;elt.vecteurY=elt.vecteurY/elt.vecteurY;}
		}
	}



/***********************************************
****		gestion des cas limites           ****
	x:elt.coordX					x:elt.coordX+elt.idCSSjq.width()
	y:elt.coordY					y:elt.coordY
	
	x:elt.coordX					x:elt.coordX+elt.idCSSjq.width()
	y:elt.coordY+elt.idCSSjq.height()	y:elt.coordY+elt.idCSSjq.height()
***********************************************/
elt_cpt_limite=
	{
	rebondiX:function(elt)
		{
		if(elt.coordX<=0){elt.coordX = 0;elt.vecteurX=-elt.vecteurX;return;}
		if(elt.coordX+elt.idCSS.width>=elt.idCSSjq_parent.width()){elt.vecteurX=-elt.vecteurX;}
		}
	,rebondiY:function(elt)
		{
		if(elt.coordY<=0){elt.coordY=0;elt.vecteurY=-elt.vecteurY;return;}
		if(elt.coordY+elt.idCSS.height>elt.idCSSjq_parent.height()){elt.vecteurY=-elt.vecteurY;}
		}

	,traverseX:function(elt)
		{
		if(elt.coordX<=0){elt.coordX=elt.idCSSjq_parent.width()-elt.idCSS.width;return;}
		if(elt.coordX+elt.idCSS.width>=elt.idCSSjq_parent.width())elt.coordX=1;
		}
	,traverseY:function(elt)
		{
		if(elt.coordY<=0){elt.coordY=elt.idCSSjq_parent.height()-elt.idCSS.height;return;}
		if(elt.coordY+elt.idCSS.height>=elt.idCSSjq_parent.height())elt.coordY=1;
		}

	,neverOut:function(elt)
		{
		if(elt.coordX+elt.idCSS.width>elt.idCSSjq_parent.width()){elt.coordX=elt.idCSSjq_parent.width()-elt.idCSS.width;}
		if(elt.coordY+elt.idCSS.height>elt.idCSSjq_parent.height()){elt.coordY=elt.idCSSjq_parent.height()-elt.idCSS.height;}
		}
	}

/***********************************************
****		gestion des affichages            ****
***********************************************/
elt_cpt_affichage=
	{affichage0:function(elt)
		{
//		elt.idCSS.css({top:elt.coordY+'px',left:elt.coordX+'px'});
		elt.idCSS.style.top=elt.coordY+'px';
		elt.idCSS.style.left=elt.coordX+'px';
		}
	}
/*!******************************************************************
fichier: ruches-0.2.2-comportement.js
version : 0.2.2
auteur : Pascal TOLEDO
date :1 decembre 2012
source: http://www.legral.fr/intersites/lib/perso/js/gestionScenario/
idee original:http://www.javascriptfr.com//code.aspx?ID=54636
description: comportement d'un element dans la ruche
compatibilite:
dependance:aucune
*******************************************************************/

/***********************************************
****		gestion des cas normaux           ****
***********************************************/
elt_cpt_normal=
	{
	  vecteurX:   function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurX=v;}
	 ,vecteurY:   function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurY=v;}
	 ,vecteurRelX:function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurX+=v;}
	 ,vecteurRelY:function(elt,v){if((!elt)&&(!v)){return;}elt.vecteurY+=v;}

	 ,vecteurAleatY:function(elt,min,max)
	 	{
	 	if((!elt)&&(!min)&&(!max)){return;}
	 	elt.vecteurY=Math.AleatoireSignedMinMax(min,max);
	 	}
	,vecteurAleatX:function(elt,min,max)
		{
	 	if((!elt)&&(!min)&&(!max)){return;}
		elt.vecteurX=Math.AleatoireSignedMinMax(min,max);
		}
	,walk:function(elt,coef)
		{
		var v=(coef)?coef:elt.vitesse;
		elt.coordX+=elt.vecteurX*v;elt.coordY+=elt.vecteurY*v;
		}
	,moveXY:function(elt,x,y)
		{
		if(!elt){return;}
		if(x){elt.coordX=x;}
		if(y){elt.coordY=y;}
		}
	,moveRelXY:function(elt,x,y)
		{
		if(!elt){return;}
		if(x){elt.coordX+=x;}
		if(y){elt.coordY+=y;}
		}
	,moveCosXY:function(elt,x,y)
		{
		if(!elt){return;}
		var cos=Math.cos(elt.age.cycle);
		if(x){elt.coordX=elt.coordX+Math.round(cos*x);}
		if(y){elt.coordY=elt.coordY+Math.round(cos*y);}
		}
	,vaVersElt:function(elt,elt2)
		{
		if((!elt)||(typeof(elt2)==='undefined')){return;}
		var eltCX =elt.larg/2; var eltCY =elt.haut/2;
		var elt2CX=elt2.larg/2;var elt2CY=elt2.haut/2;
		
		elt.vecteurX= Math.round(  (elt2.coordX+elt2CX)-(elt.coordX+eltCX)  )   ;
		elt.vecteurY= Math.round(  (elt2.coordY+elt2CY)-(elt.coordY+eltCY)  )   ;
		if(!elt.vecteurX){elt.vecteurX=0;if(!elt.vecteurY){elt.vecteurY=0}return};
		if(!elt.vecteurY){elt.vecteurY=0;if(!elt.vecteurX){elt.vecteurX=0}return};
		//ratio
		if     (elt.vecteurX<elt.vecteurY){elt.vecteurY=elt.vecteurY/elt.vecteurX;elt.vecteurX=elt.vecteurX/elt.vecteurX;}
		else if(elt.vecteurX>elt.vecteurY){elt.vecteurX=elt.vecteurX/elt.vecteurY;elt.vecteurY=elt.vecteurY/elt.vecteurY;}
		else {elt.vecteurX=elt.vecteurX/elt.vecteurX;elt.vecteurY=elt.vecteurY/elt.vecteurY;}
		}
	}



/***********************************************
****		gestion des cas limites           ****
	x:elt.coordX					x:elt.coordX+elt.idCSSjq.width()
	y:elt.coordY					y:elt.coordY
	
	x:elt.coordX					x:elt.coordX+elt.idCSSjq.width()
	y:elt.coordY+elt.idCSSjq.height()	y:elt.coordY+elt.idCSSjq.height()
***********************************************/
elt_cpt_limite=
	{
	rebondiX:function(elt)
		{
		if(elt.coordX<=0){elt.coordX = 0;elt.vecteurX=-elt.vecteurX;return;}
		if(elt.coordX+elt.idCSS.width>=elt.idCSSjq_parent.width()){elt.vecteurX=-elt.vecteurX;}
		}
	,rebondiY:function(elt)
		{
		if(elt.coordY<=0){elt.coordY=0;elt.vecteurY=-elt.vecteurY;return;}
		if(elt.coordY+elt.idCSS.height>elt.idCSSjq_parent.height()){elt.vecteurY=-elt.vecteurY;}
		}

	,traverseX:function(elt)
		{
		if(elt.coordX<=0){elt.coordX=elt.idCSSjq_parent.width()-elt.idCSS.width;return;}
		if(elt.coordX+elt.idCSS.width>=elt.idCSSjq_parent.width())elt.coordX=1;
		}
	,traverseY:function(elt)
		{
		if(elt.coordY<=0){elt.coordY=elt.idCSSjq_parent.height()-elt.idCSS.height;return;}
		if(elt.coordY+elt.idCSS.height>=elt.idCSSjq_parent.height())elt.coordY=1;
		}

	,neverOut:function(elt)
		{
		if(elt.coordX+elt.idCSS.width>elt.idCSSjq_parent.width()){elt.coordX=elt.idCSSjq_parent.width()-elt.idCSS.width;}
		if(elt.coordY+elt.idCSS.height>elt.idCSSjq_parent.height()){elt.coordY=elt.idCSSjq_parent.height()-elt.idCSS.height;}
		}
	}

/***********************************************
****		gestion des affichages            ****
***********************************************/
elt_cpt_affichage=
	{affichage0:function(elt)
		{
//		elt.idCSS.css({top:elt.coordY+'px',left:elt.coordX+'px'});
		elt.idCSS.style.top=elt.coordY+'px';
		elt.idCSS.style.left=elt.coordX+'px';
		}
	}
