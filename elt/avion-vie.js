/*******************************************************************
// avion
*******************************************************************/
function elt_avion_load(elt)// a transformer en elt.
{
//----[collision]----//
elt.cpt.fct['collision']=function(elt,data)
	{
	if(!data){data={};}
	if(!elt.cpt.iteration['collision']){elt.cpt.iteration['collision']=0;}elt.cpt.iteration['collision']++;
/*
	if(elt.cpt.actionFlag['ecrasement']!==1)
		{
		if(!data.obstacle)
			{
			return;
			}
		}
*/
	if(data.obstacle)
		{
		switch(data.obstacle.genre)
			{case'panneau':
				elt.cpt.CeC='ecrasement';
				elt.cpt.actionFlag['ecrasement']=1;
				elt.cpt.fct['ecrasement'](elt);
				break;
			case 'abeille':
				elt.cpt.CeC=0;
				elt.cpt.actionFlag['ecrasement']=0;
				break;
			}
		}
	}

elt.cpt.fct[0]=function(elt)
	{
	if(!elt.cpt.iteration[0])
		{
		elt.cpt.iteration[0]=0;
		gestLib.write({lib:'dimention',txt:'avion: en vol'});
		}
	elt.cpt.iteration[0]++;
	if(elt.statut<0){return elt.statut;}
	//[age]
	if(elt.age.cycle>360){elt.age.cycle=0;elt.age.cycleNb++;}
	//[normal]
	elt_cpt_normal.moveRelXY(elt,-5,0);
	elt_cpt_normal.walk(elt);
	//[limite]
	elt_cpt_limite.traverseY(elt);
	elt_cpt_limite.traverseX(elt);
	elt_cpt_limite.neverOut(elt);
	//[affichage]
	elt_cpt_affichage.affichage0(elt);
	//[condition de changement de comportement]
//	if(elt.age.cycle>=40)		{elt.cpt.CeC='ecrasement';		}
	}
elt.cpt.fct['ecrasement']=function(elt)
	{
	if(!elt.cpt.iteration['ecrasement'])
		{
		elt.cpt.iteration['ecrasement']=0;
		gestLib.write({lib:'dimention',txt:'avion: debut de l\'ecrasement!'});
		}
	elt.cpt.iteration['ecrasement']++;

	if(elt.statut<0){return elt.statut;}
	//[age]
	if(elt.age.cycle>360){elt.age.cycle=0;elt.age.cycleNb++;}
	//[normal]
	elt_cpt_normal.moveRelXY(elt,-5,1);
	elt_cpt_normal.moveCosXY(elt,0,10);
	//elt_cpt_normal.walk(elt);
	//[limite]
	elt_cpt_limite.traverseX(elt);
	elt_cpt_limite.neverOut(elt);
	//[affichage]
	elt_cpt_affichage.affichage0(elt);
	//[condition de changement de comportement]
	if(elt.coordY+elt.idCSS.height>=elt.idCSSjq_parent.height())
		{elt.cpt.CeC='die';
		}
	}
elt.cpt.fct['die']=function(elt)
	{
	if(!elt.cpt.iteration['die'])
		{
		elt.cpt.iteration['die']=0;
		gestLib.write({lib:'dimention',txt:'avion: mort!'});
		}
	elt.cpt.iteration['die']++;

	if(elt.statut<0)
		{
		gestLib.write({lib:'dimention',txt:'avion:deja mort!'});
		return elt.statut;
		}
	//[age]
	if(elt.age.cycle>360){elt.age.cycle=0;elt.age.cycleNb++;}
	//[normal]
	//[limite]
	elt_cpt_limite.traverseX(elt);

	//[affichage]
	//elt.idCSSjq.css({display:'none'});
	elt_cpt_affichage.affichage0(elt);
	elt.idCSS.src='/intersites/lib/perso/js/dimentions/img/14671494-croix-noire-sur-fond-grunge.png';
	elt.idCSS.style.width=elt.idCSS.width;
	elt.idCSS.style.height=elt.idCSS.height;
	elt.statut=-1;//mort!
	gestLib.write({lib:'dimention',txt:'avion:mort!'});
	//[condition de changement de comportement]

	}
}//function elt_avion_init()
